#include <memory>
#include "class.hpp"




int main() {

    auto Teacher_Tom{std::make_shared<Teacher>("Teacher_Tom") };
    auto Teacher_John{ std::make_shared<Teacher>("Teacher_John") };
    auto Student_Max{std::make_shared<Student>("Student_Max") };
    auto Student_Lilly{std::make_shared<Student>("Student_Lilly") };
    auto Student_Emily{ std::make_shared<Student>("Student_Emily") };

    AddStudents(Teacher_Tom, Student_Max);
    AddStudents(Teacher_Tom, Student_Lilly);
    AddStudents(Teacher_John, Student_Emily);

    Teacher_John->ShowStudentsList();
    Teacher_Tom->ShowStudentsList();

    return 0;
}
