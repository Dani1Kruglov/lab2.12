//
// Created by Danil on 20.09.2022.
//

#ifndef LAB2_12_CLASS_HPP
#define LAB2_12_CLASS_HPP

#endif //LAB2_12_CLASS_HPP
#include <iostream>
#include <vector>
#include <memory>

class Teacher;

class Student {
public:

    Student(const std::string& name)  {
        m_student_name = name;
        std::cout << m_student_name << " created" << std::endl;
    }
    ~Student() {
        std::cout<< m_student_name << " destroyed" << std::endl;
    }

    const std::string& GetStudentName() const {
        return m_student_name;
    }

    const std::weak_ptr<Teacher> GetTeacher() {
        return m_teacher;
    }
    friend void AddStudents(std::shared_ptr<Teacher>& Teacher, std::shared_ptr<Student>& Student);

private:
    std::string m_student_name;
    std::weak_ptr<Teacher> m_teacher;
};

class Teacher {
public:
    Teacher(const std::string& name) {
        m_teacher_name = name;
        std::cout << "Teacher " << m_teacher_name << " created" << std::endl;
    }
    ~Teacher() {
        std::cout << "Teacher " << m_teacher_name << " destroyed" << std::endl;
    }
    void AddStudent(std::shared_ptr<Student>& Student) {
        m_students.push_back(Student);
    }

    void ShowStudentsList() {
        std::cout << " " << std::endl;
        std::cout << "Students list of "<< m_teacher_name<< ":" << std::endl;
        for (auto const& student : m_students)
            std::cout << student->GetStudentName() << std::endl;
        std::cout << " " << std::endl;
    }
    const std::string& GetTeacherName() const {
        return m_teacher_name;
    }
    friend void AddStudents(std::shared_ptr<Teacher>& Teacher, std::shared_ptr<Student>& Student);

private:
    std::string m_teacher_name;
    std::vector<std::shared_ptr<Student>> m_students;

};


void AddStudents(std::shared_ptr<Teacher>& Teacher, std::shared_ptr<Student>& Student) {
    Student->m_teacher = Teacher;
    Teacher->AddStudent(Student);
}